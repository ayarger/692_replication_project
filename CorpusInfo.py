import xml.etree.ElementTree as ET
from collections import OrderedDict
from DocumentInfo import DocumentInfo
from AuthorInfo import AuthorInfo
from yprint import yprint
from Config import Config

class CorpusInfo:
    def __init__(self):
        # Data
        self.documents = []
        self.authors = OrderedDict() # author_id -> author_info
        self.vocabulary_size = 0
        self.ordered_vocabulary = OrderedDict()
            
    def _calculate_vocab(self):
        if len(self.documents) <= 0:
            fprint ("ERROR: You need to run _initialize_documents first!")
        
        word_union = set()
        for doc in self.documents:  
            word_union.update(doc.ordered_word_list)
        self.vocabulary_size = len(word_union)
        vocab_list = list(word_union)
        vocab_list.sort()
        vocab_index = 0
        for word in vocab_list:
            self.ordered_vocabulary[word] = vocab_index
            vocab_index += 1
            
        yprint("VOCAB LENGTH: " + str(len(self.ordered_vocabulary)))
        
            
    def number_of_authors (self):
        return len(self.authors)
        
    def number_of_documents (self):
        return len(self.documents)
        
    def finalize(self):
        self._calculate_vocab ()
        self.index_to_vocab = OrderedDict((v,k) for k,v in self.ordered_vocabulary.items())
    