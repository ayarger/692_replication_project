"""
Datasets available...
http://www.csse.monash.edu/research/umnl/data/index.shtml
http://pan.webis.de/data.html
"""

import sys
import os.path
import time
from sklearn import svm
from scipy.stats import itemfreq
from numpy import ndarray, zeros
import numpy as np
import math
import multiprocessing as mp
import matplotlib.pyplot as plt

from collections import OrderedDict
from random import uniform

import json
from yprint import yprint, fprint, setverbosity, flush_printing
from BaseModel import BaseModel
from TokenFrequencyModel import TokenFrequencyModel
from LDAModel import LDAModel
from ATModel import ATModel
from DADTModel import DADTModel
from SVMMethod import SVMMethod
from PMethod import PMethod
from AuthorInfo import AuthorInfo
from DocumentInfo import DocumentInfo
from CorpusInfo import CorpusInfo
from Config import Config, get_filename
from JudgementParser import JudgementParser
from PAN11Parser import PAN11Parser
    
def acquire_beta_prior(corpus, stopwords):
    """
    Obtain the beta priors, encoding our belief that words favor a 
    """
    beta_prior_d = zeros(corpus.vocabulary_size)
    beta_prior_a = zeros(corpus.vocabulary_size)
    
    index = 0
    for word in corpus.ordered_vocabulary:
        beta_prior_d[index] = 0.01
        beta_prior_a[index] = 0.01
        
        if word in stopwords:
            beta_prior_d[index] -= epsilon
            beta_prior_a[index] += epsilon
            
        index += 1
        
    return beta_prior_d, beta_prior_a
    
def acquire_alpha_prior():
    return min (0.1, 5.0 / num_document_topics), min(0.1, 5.0 / num_author_topics)
       

    
def GenerateGraphs(test_objects, results):
    # LDA Graphs (Figure 5 from Seroussi et al.) ######################################
    
    # Prepare results
    results = np.array(results) * 100
    
    #gen_sec_2_1(test_objects, results)
    #gen_sec_2_2(test_objects, results)
    #gen_sec_3_1(test_objects, results)
    #gen_sec_3_2(test_objects, results)
    #gen_sec_4_1(test_objects, results)
    
    
def gen_sec_2_1(test_objects, results):
    # Prepare results 
    plt.xlabel('Number of Topics')
    plt.ylabel('Accuracy [%]')
    
    token_value = 0
    tests = []
    
    ind = 0
    for t in test_objects:
        if t["model"] == "lda" and t["parser"] == "judgement":
            tests.append((t, results[ind]))
        ind += 1
        
    ind = 0
    for t in test_objects:
        if t["model"] == "token_frequency" and t["parser"] == "judgement":
            token_value = results[ind]
        ind += 1
    
    x1 = [int(tests[i][0]["num_topics"]) for i in range(len(tests))]
    y1 = [tests[i][1] for i in range(len(tests))]
    
    print("token value: " + str(token_value))
    baseline = plt.axhline(y=token_value, xmin=0, xmax=1, hold=None, c="red")
    
    #plt.plot(t, t, '--bo', t, t**2, 'bs', t, t**3, 'g^')
    plt.yticks(np.arange(0, 110, 10.0))
    lda_svm, = plt.plot(x1, y1, '--bo')
    
    plt.legend([baseline, lda_svm], ["Token SVM", "LDA-SVM"])
    
    if len(x1) == 0:
        return
    plt.savefig(get_filename() + "_lda.png")
    
def gen_sec_2_2(test_objects, results):
    # Prepare results 
    plt.xlabel('Number of Topics')
    plt.ylabel('Accuracy [%]')
    
    token_value = 0
    tests = []
    
    ind = 0
    for t in test_objects:
        if t["model"] == "lda" and t["parser"] == "pan11":
            tests.append((t, results[ind]))
        ind += 1
        
    ind = 0
    for t in test_objects:
        if t["model"] == "token_frequency" and t["parser"] == "pan11":
            token_value = results[ind]
        ind += 1
    
    x1 = [int(tests[i][0]["num_topics"]) for i in range(len(tests))]
    y1 = [tests[i][1] for i in range(len(tests))]
    
    print("token value: " + str(token_value))
    baseline = plt.axhline(y=token_value, xmin=0, xmax=1, hold=None, c="red")
    
    #plt.plot(t, t, '--bo', t, t**2, 'bs', t, t**3, 'g^')
    plt.yticks(np.arange(0, 110, 10.0))
    lda_svm, = plt.plot(x1, y1, '--bo')
    
    plt.legend([baseline, lda_svm], ["Token SVM", "LDA-SVM"])
    
    if len(x1) == 0:
        return
    plt.savefig(get_filename() + "_lda.png")
    
def gen_sec_4_1(test_objects, results):
    # Prepare results 
    plt.xlabel('Number of Topics')
    plt.ylabel('Accuracy [%]')
    
    token_value = 0
    svm_tests = []
    p_tests = []
    
    ind = 0
    for t in test_objects:
        if t["model"] == "dadt" and t["parser"] == "judgement" and t["method"] == "svm":
            svm_tests.append((t, results[ind]))
        ind += 1
        
    ind = 0
    for t in test_objects:
        if t["model"] == "dadt" and t["parser"] == "judgement" and t["method"] == "p":
            p_tests.append((t, results[ind]))
        ind += 1
        
    ind = 0
    for t in test_objects:
        if t["model"] == "token_frequency" and t["parser"] == "judgement":
            token_value = results[ind]
        ind += 1
    
    x1 = [int(svm_tests[i][0]["num_topics"]) for i in range(len(svm_tests))]
    y1 = [svm_tests[i][1] for i in range(len(svm_tests))]
    
    x2 = [int(p_tests[i][0]["num_topics"]) for i in range(len(p_tests))]
    y2 = [p_tests[i][1] for i in range(len(p_tests))]
    
    print("token value: " + str(token_value))
    baseline = plt.axhline(y=token_value, xmin=0, xmax=1, hold=None, c="red")
    
    #plt.plot(t, t, '--bo', t, t**2, 'bs', t, t**3, 'g^')
    plt.yticks(np.arange(0, 110, 10.0))
    lda_svm, lda_p, = plt.plot(x1, y1, '--ro', x2, y2, '--gs')
    
    plt.legend([baseline, lda_svm], ["Token SVM", "AT-SVM", "AT-P"])
    
    if len(x1) == 0 and len(x2) == 0:
        return
    
    plt.savefig(get_filename() + "_at.png")
    
def gen_sec_3_1(test_objects, results):
    # Prepare results 
    plt.xlabel('Number of Topics')
    plt.ylabel('Accuracy [%]')
    
    token_value = 0
    svm_tests = []
    p_tests = []
    
    ind = 0
    for t in test_objects:
        if t["model"] == "at" and t["parser"] == "judgement" and t["method"] == "svm":
            svm_tests.append((t, results[ind]))
        ind += 1
        
    ind = 0
    for t in test_objects:
        if t["model"] == "at" and t["parser"] == "judgement" and t["method"] == "p":
            p_tests.append((t, results[ind]))
        ind += 1
        
    ind = 0
    for t in test_objects:
        if t["model"] == "token_frequency" and t["parser"] == "judgement":
            token_value = results[ind] * 100
        ind += 1
    
    token_value = 0.37711244913871356 * 100
    
    x1 = [int(svm_tests[i][0]["num_topics"]) for i in range(len(svm_tests))]
    y1 = [svm_tests[i][1] for i in range(len(svm_tests))]
    
    x2 = [int(p_tests[i][0]["num_topics"]) for i in range(len(p_tests))]
    y2 = [p_tests[i][1] for i in range(len(p_tests))]
    
    print("token value: " + str(token_value))
    baseline = plt.axhline(y=token_value, xmin=0, xmax=1, hold=None, c="red")
    
    #plt.plot(t, t, '--bo', t, t**2, 'bs', t, t**3, 'g^')
    plt.yticks(np.arange(0, 110, 10.0))
    lda_svm, lda_p, = plt.plot(x1, y1, '--ro', x2, y2, '--gs')
    
    plt.legend([baseline, lda_svm, lda_p], ["Token SVM", "AT-SVM", "AT-P"])
    
    if len(x1) == 0 and len(x2) == 0:
        return
    plt.savefig(get_filename() + "_at.png")
    
def gen_sec_3_2(test_objects, results):
    # Prepare results 
    plt.xlabel('Number of Topics')
    plt.ylabel('Accuracy [%]')
    
    token_value = 0
    svm_tests = []
    p_tests = []
    
    ind = 0
    for t in test_objects:
        if t["model"] == "at" and t["parser"] == "pan11" and t["method"] == "svm":
            svm_tests.append((t, results[ind]))
        ind += 1
        
    ind = 0
    for t in test_objects:
        if t["model"] == "at" and t["parser"] == "pan11" and t["method"] == "p":
            p_tests.append((t, results[ind]))
        ind += 1
        
    ind = 0
    for t in test_objects:
        if t["model"] == "token_frequency" and t["parser"] == "pan11":
            token_value = results[ind]
        ind += 1
    
    
    
    x1 = [int(svm_tests[i][0]["num_topics"]) for i in range(len(svm_tests))]
    y1 = [svm_tests[i][1] for i in range(len(svm_tests))]
    
    x2 = [int(p_tests[i][0]["num_topics"]) for i in range(len(p_tests))]
    y2 = [p_tests[i][1] for i in range(len(p_tests))]
    
    print("token value: " + str(token_value))
    baseline = plt.axhline(y=token_value, xmin=0, xmax=1, hold=None, c="red")
    
    #plt.plot(t, t, '--bo', t, t**2, 'bs', t, t**3, 'g^')
    plt.yticks(np.arange(0, 110, 10.0))
    lda_svm, lda_p, = plt.plot(x1, y1, '--ro', x2, y2, '--gs')
    
    plt.legend([baseline, lda_svm, lda_p], ["Token SVM", "AT-SVM", "AT-P"])
    
    if len(x1) == 0 and len(x2) == 0:
        return
    plt.savefig(get_filename() + "_at.png")
    
def run_test(test, test_number):

    # Stopwords config
    Config.stop_words = not test["stopwords"]
    
    # MISC Config
    verbosity = test["verbose"]
    setverbosity(verbosity)
    
    # Corpus config
    training_dataset_name = test["training_dataset"]
    parser_name = test["parser"]
    
    # Testing config
    testing_dataset_name = test["testing_dataset"]
    
    # Cross Validation Config
    num_cv_folds = test["num_cv_folds"]
    
    # Model Config
    model_name = test["model"]
    
    # Method config
    method_name = test["method"]

    # Let the printing system know about the experiment params
    Config.test_number = test_number
    Config.model = model_name
    Config.method = method_name
    Config.iterations = test["num_iterations"]
    if test["model"] == "dadt":
        Config.num_topics = test["num_document_topics"] + test["num_author_topics"]
    else:
        Config.num_topics = test["num_topics"]

    # Build Corpus
    training_corpus = CorpusInfo ()
    parser = None
    if parser_name == "judgement":
        parser = JudgementParser(test["training_dataset"])
    elif parser_name == "pan11":
        parser = PAN11Parser(test["training_dataset"])
        
    if parser == None:
        fprint("[ERROR] Parser with name [" + parser_name + "] does not exist.")
        return
        

        
    training_corpus = parser.get_corpus()
    
    # Train Model
    model = None
    if model_name == "token_frequency":
        model = TokenFrequencyModel(training_corpus)
    elif model_name == "lda":
        model = LDAModel(training_corpus, test["num_topics"], test["num_iterations"])
    elif model_name == "at":
        model = ATModel(training_corpus, test["num_topics"], test["num_iterations"])
    elif model_name == "dadt":
        model = DADTModel(training_corpus, test["num_document_topics"], test["num_author_topics"], test["num_iterations"])
    
    if model == None:
        fprint("[ERROR] Model with name [" + model_name + "] does not exist.")
        return
    
    fprint ("RUNNING TEST [" + model_name + "] with method [" + test["method"] + "] on training data [" + training_dataset_name + "] and testing data [" + testing_dataset_name + "] stopwords: [" + str(Config.stop_words) + "]")
    
    print ("Initializing model...")
    model.initialize()
    print ("...done!")
    
    # Build Method
    method = None
    if method_name == "svm":
        method = SVMMethod(model)
    elif method_name == "p":
        method = PMethod(model)
        
    if method == None:
        fprint ("[ERROR] method with name [" + method_name + "] not found.")
        return
    
    validation_score = None
    if parser_name == "judgement" or True:
        validation_score = method.validate(num_cv_folds)
    elif parser_name == "pan11":
        other_corpus = parser.get_validation_corpus()
        validation_score = method.validate_on_other_corpus(other_corpus)
    
    fprint ("Validation (" + str(num_cv_folds) + " folds): " + str(validation_score))
    fprint("")
    flush_printing()
    return validation_score
    
def ReportAvgs():
    jp = JudgementParser("tokenised.txt").get_corpus()
    
    pp = PAN11Parser("pan11-author-identification-training-corpus-2011-04-08/LargeTrain.xml").get_corpus()

    num_words_jp = 0
    for doc in jp.documents:
        num_words_jp += doc.number_of_words
        
    num_words_pp = 0
    for doc in pp.documents:
        num_words_pp += doc.number_of_words
    
    print ("Judgement words avg: " + str(num_words_jp / len(jp.documents)))
    print ("PAN'11 words avg: " + str(num_words_pp / len(pp.documents)))
    
def main():

    
    
    
    

    run_number = 0

    if not os.path.isfile("latest_run_number"):
        with open("latest_run_number", "w+") as run_file:
            run_file.write(str(run_number))

    with open("latest_run_number", "r+") as run_file:
        latest_run_number = run_file.read()
        print ("read: " + latest_run_number)
        if latest_run_number.isdigit():
            run_number = int(latest_run_number) + 1
        
        
    with open("latest_run_number", "w+") as run_file:
        run_file.write(str(run_number))
    
    Config.run_number = run_number
    
    config_file = "..."
    
    if len(sys.argv) != 2:
        print ("USAGE: \"python main.py <exp_config_file.json>\"")
        return
    else:
        config_file = sys.argv[1]

    test_objects = list()

    # Load config
    with open(config_file) as data_file:    
        data = json.load(data_file)
        for test in data["tests"]:
            test_objects.append(test)
    
    # Run tests
    print ("\n===" + str(len(test_objects)) + " TESTS FOUND===\n")
    pool = mp.Pool(processes=len(test_objects))
    results = [pool.apply_async(run_test, args=(test_objects[i],i)) for i in range(len(test_objects))]
    
    output = [p.get() for p in results]
    
    print ("Finished!" + str(output))
    print ("\a")
    
    GenerateGraphs(test_objects, output)
    
    
if __name__ == "__main__":
    start_time = time.time()
    main()
    print ("Wall Clock Running Time:" + str(time.time() - start_time) + " seconds")