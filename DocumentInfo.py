import string
from Config import Config
from stop_words import get_stop_words 

class DocumentInfo:
    next_numeric_id = 0
    
    def __init__(self, document_id, text_content):
        # Data
        self.id = document_id
        self.numeric_id = DocumentInfo.next_numeric_id
        DocumentInfo.next_numeric_id += 1
        self.author_ids = []
        self.number_of_words = 0
        self.ordered_word_list = []
      
        # Init
        self._calculate_word_list(text_content)
        
    def _calculate_word_list (self, text_content):
        stop_words_list = get_stop_words('english')
        self.ordered_word_list = [word.strip(string.punctuation) for word in text_content.split()]
        #v = list(set([x.lower() for x in self.ordered_word_list]) - set(get_stop_words("english")))
        v = list()
        if Config.stop_words:
            v = list(filter(lambda a: a != "", self.ordered_word_list))
        else:
            v = list(filter(lambda a: a != "" and a not in stop_words_list, self.ordered_word_list))
            
        self.ordered_word_list = v
        self.ordered_word_list = self.ordered_word_list[:Config.document_size_limit]
        self.ordered_word_list.sort()
        self.number_of_words = len(self.ordered_word_list)
        
class DocumentParamsInfo:
    def __init__(self, num_document_topics=5, doc_topic_prior=[], word_in_doc_topic_prior=[], doc_words_in_doc_prior=5):
        self.num_document_topics = num_document_topics
        self.doc_topic_prior = doc_topic_prior
        self.word_in_doc_topic_prior = word_in_doc_topic_prior
        self.doc_words_in_doc_prior = doc_words_in_doc_prior