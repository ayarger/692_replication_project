import time
from stop_words import get_stop_words

class Config:
    epsilon = 0.009
    delta_d = 1.222
    delta_a = 4.889
    mu_a = 1.0
    document_count_limit = 99999999
    document_size_limit = 500
    stop_words = False
    stop_words_list = get_stop_words ("english")
    run_number = 0
    test_number = 0
    
    model = None
    method = None
    iterations = None
    num_topics = None
    
def get_filename():
    filename = "run_" + str(Config.run_number) + "-" + str(Config.test_number) + "_"
        
    if Config.model != None:
        filename += Config.model + "_"
        
    if Config.method != None:
        filename += Config.method + "_"
        
    if Config.iterations != None:
        filename += "iters_" + str(Config.iterations) + "_"
        
    if Config.num_topics != None:
        filename += "topics_" + str(Config.num_topics) + "_"

    filename += "dt_" + str(time.strftime("%m_%d_%y_%H_%M"))
        
    return filename
    
