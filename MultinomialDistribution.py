class MultinomialDistribution:
    # Info: http://bit.ly/2oytyq7
    
    def __init__(self, num_trials, event_probabilities):
        self._num_trials = num_trials
        self._event_probabilities = event_probabilities
        
        # Event probabilities must sum to 1.0
        check_value = 0
        for p in event_probabilities:
            check_value += p
        assert(check_value == 1.0)
        
    def query(self, event_counts):
        assert (len(event_counts) == len(self._event_probabilities))
        numerator = math.factorial(self._num_trials)
        denominator = 1
        multiplication_part = 1
        for i in range(len(event_counts)):
            denominator *= math.factorial (event_counts[i])
            multiplication_part *= pow(self._event_probabilities[i], event_counts[i])
        
        result = numerator / denominator
        result *= multiplication_part
        
        return result