import numpy as np
from collections import Counter, OrderedDict
from yprint import yprint, fprint
from BaseModel import BaseModel
import time

# PARAMS
smoothing = 0.1

class LDAModel(BaseModel):      
    
    def __init__(self, corpus, num_topics, num_iterations):
        self.corpus = corpus
        self.num_topics = num_topics
        self.num_iterations = num_iterations
        self.done_iterating = False
        
        self.word_topic_counts = np.zeros(shape=(corpus.vocabulary_size, num_topics))
        
        self.topic_counts = np.zeros(shape=(len(corpus.documents), num_topics))
        self.word_topic_assignments = list()
        for i in range(len(corpus.documents)):
            self.word_topic_assignments.append(np.zeros(corpus.documents[i].number_of_words))
  
    def get_description_string():
        return "num_topics: [" + str() + "]"; 

    def initialize(self):
        # Initialization
        yprint ("[BEGIN LDA]")
        yprint ("Initialization...")
        tim = time.time()
        
        for d in range(len(self.corpus.documents)):
            num_words_in_doc = self.corpus.documents[d].number_of_words
            ind = 0
            #print(str(len(self.corpus.documents[d].ordered_word_list)))
            for word in self.corpus.documents[d].ordered_word_list:
                # Randomly assign word to topic.
                t = np.random.randint (self.num_topics)
                
                self.word_topic_assignments[d][ind] = t
                self.topic_counts[d][t] += 1
                
                # Global increment
                self.word_topic_counts[self.corpus.ordered_vocabulary[word]][t] += 1
                    
                ind += 1
    
        print("FINISHED SETUP IN: " + str(time.time() - tim))
    
        yprint("ALL SET UP!")
        
    def get_training_data(self):
        if not self.done_iterating:
            self.perform_iteration(self.corpus, self.num_topics, self.num_iterations)
        
        X = list()
        y = list()
        for d in range(len(self.corpus.documents)):
            X.append(self.topic_counts[d] / sum(self.topic_counts[d]))
            y.append(self.corpus.documents[d].author_ids[0])
        
        return X, y
        

        
    def GetTopicDistroForDocument (doc_index):
        norm = self.topic_counts[doc_index]
        if(sum(norm) > 0):
            norm = [float(i)/sum(self.topic_counts[doc_index]) for i in self.topic_counts[doc_index]]
        return norm
       
    def GetWordDistroForTopic (corpus, topic_index):
        c = self.word_topic_counts[:, topic_index]
        norm = c
        if(sum(c) > 0):
            norm = [float(i)/sum(c) for i in c]
        
        pair_list = list()
        index = 0
        for a in norm:
            pair_list.append((a, index))
            index += 1
        pair_list.sort(reverse=True)
        
        # turn indices into strings
        pair_string_list = list()
        for p in pair_list:
            pair_string_list.append((p[0], corpus.index_to_vocab[p[1]]))
        
        pair_string_list = pair_string_list[:5]
        return pair_string_list
        
    def perform_iteration(self, corpus, num_topics, num_iterations):
        # Setup
        print ("[Iteration Process] iters:" + str(num_iterations) + " docs:" + str(len(corpus.documents)) + " vocab:" + str(len(corpus.ordered_vocabulary)))  
        print ("Iterating Model...")
        
        temp_corpus_topic_counts = np.zeros(num_topics)
        for potential_topic in range(num_topics):
            temp_corpus_topic_counts[potential_topic] = self.word_topic_counts[:,potential_topic].sum()
        
        previous_timestamp = time.time()
        
        print("caches setup")
        
        int_cast = int
        
        # Data Stores
        potentials = np.zeros(num_topics)
        topics_range = range(num_topics)
        
        
        # Iteration
        for iteration in range(num_iterations):
            temp_ind = 0
            for d in range(len(corpus.documents)):
                #print ("Doc " + str(temp_ind) + " of " + str(len(corpus.documents)))
                temp_ind += 1
                
                topic_counts_for_document_cache = self.topic_counts[d]
                doc = corpus.documents[d]
                num_words_in_doc = doc.number_of_words
                
                for i in range(num_words_in_doc):
                    
                    # Remove assignment.
                    word = doc.ordered_word_list[i]
                    word_topic_counts_for_word = self.word_topic_counts[corpus.ordered_vocabulary[word]]
                    current_topic = int_cast(self.word_topic_assignments[d][i])
                    self.word_topic_assignments[d][i] = -1
                    topic_counts_for_document_cache[current_topic] -= 1

                    word_topic_counts_for_word[current_topic] -= 1
                    temp_corpus_topic_counts[current_topic] -= 1
                    #print("Word: " + word + " at index " + str(corpus.ordered_vocabulary[word]))
                    
                    
                    
                    
                    # Choose new assignment
                    # Iterate through topics
                    # TODO: missing smoothing param.
                    
                    
                    doc_likes_topic = (topic_counts_for_document_cache + smoothing) / (num_words_in_doc - 1 + num_topics * smoothing)
                    denom_sum = temp_corpus_topic_counts
                    topic_likes_word = word_topic_counts_for_word + smoothing / (denom_sum + smoothing * corpus.vocabulary_size)
                    
                    potentials = doc_likes_topic * topic_likes_word
                    
                    
                    # Apply new topic assignment
                    potentials = potentials / potentials.sum()
                    new_topic = int_cast(np.random.choice(topics_range, p=potentials))
                    self.word_topic_assignments[d][i] = new_topic
                    topic_counts_for_document_cache[new_topic] += 1
                    temp_corpus_topic_counts[new_topic] += 1
                    word_topic_counts_for_word[new_topic] += 1
                    
                    
            seconds_per_iteration = time.time() - previous_timestamp
            previous_timestamp = time.time()
            est_seconds_remaining = seconds_per_iteration * (num_iterations - iteration)
            est_minutes_remaining = int_cast(est_seconds_remaining / 60)
            est_seconds_remaining = est_seconds_remaining % 60
            print ("Finished iteration " + str(iteration) + " / " + str(num_iterations) + " ETA: " +str(est_minutes_remaining) + "m " + str(est_seconds_remaining) + "s") 
        self.done_iterating = True
