from random import uniform
import math

class DirichletDistribution:
    """
    A distribution 
    # Info: https://www.wikiwand.com/en/Dirichlet_distribution
    """
    def __init__(self, concentration_params):
        self._num_categories = len(concentration_params)
        for concentration_param in concentration_params:
            assert(concentration_param > 0)
        
        self._concentration_params = concentration_params
        
    def query(self, event_counts):
        result = 1
        for i in range(self._num_categories):
            result *= pow(event_counts[i], self._concentration_params[i] - 1)
        
        # TODO: figure out how to implement Beta Function Normalizer.
        beta_result = beta(event_counts)
        assert(beta_result != 0)
        result /= beta_result
        
        return result
        
def beta(alpha):
        """
            Multivariate Beta Function
            Info: http://bit.ly/2nW8H1B
            - alpha: list of values
        """
        numerator = 1
        for a in alpha:
            numerator *= gamma(a)
        
        gamma_input = 0
        for a in alpha:
            gamma_input += a
        denominator = gamma(gamma_input)
        
        return numerator / denominator
        
def gamma(n):
        """
            Gamma Function
            - n: a value
            Info: https://www.wikiwand.com/en/Gamma_function
        """
        return math.factorial(n-1)
        
def bernoulli(f):
    if uniform(0.0, 1.0) <= f:
        return 1
    return 0