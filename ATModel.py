"""
Z (word-to-topic assignments) are the exact same as in LDA.
Phi (word_topic_counts) is the exact same as in LDA.
Theta (topic_counts) is across authors now. So shape = (num_authors, mum_topics)
"""


import numpy as np
import time
from collections import Counter, OrderedDict
from yprint import yprint, fprint
from BaseModel import BaseModel

# PARAMS
smoothing = 0.1

class ATModel(BaseModel):      
    
    def __init__(self, corpus, num_topics, num_iterations):
        self.corpus = corpus
        self.num_topics = num_topics
        self.num_iterations = num_iterations
        self.done_iterating = False
        
        # Phi Setup
        self.phi = np.zeros(shape=(corpus.vocabulary_size, num_topics))
        
        # Theta Setup
        self.theta = {} 
        for author_id in corpus.authors.keys():
            self.theta[author_id] = np.zeros(num_topics)
        
        # Z Setup
        self.z = list()
        for i in range(len(corpus.documents)):
            self.z.append(np.zeros(corpus.documents[i].number_of_words))
  
    def get_description_string():
        return "num_topics: [" + str() + "]"; 

    def initialize(self):
        # Initialization
        yprint ("[BEGIN AT]")
        yprint ("Initialization...")
        for d in range(len(self.corpus.documents)):
            for i in range(self.corpus.documents[d].number_of_words):
                # Update Z
                t = np.random.randint(self.num_topics)
                self.z[d][i] = t
                
                # Update Theta
                author_id = self.corpus.documents[d].author_ids[0]
                self.theta[author_id][t] += 1
                
                # Update Phi
                word = self.corpus.documents[d].ordered_word_list[i]
                self.phi[self.corpus.ordered_vocabulary[word]][t] += 1
    
        yprint("AT Setup Complete!")
        
    def get_training_data(self):
        """
        X: get author topic distro for each document.
        """
        if not self.done_iterating:
            self.perform_iteration(self.corpus, self.num_topics, self.num_iterations)
        
        X = list()
        y = list()
        for d in range(len(self.corpus.documents)):
            author_id = self.corpus.documents[d].author_ids[0]
            
            X.append(self.theta[author_id] / sum(self.theta[author_id]))
            y.append(author_id)
        
        return X, y
        
    def perform_iteration(self, corpus, num_topics, num_iterations):
        # Setup
        print ("Iteration...(" + str(num_topics) + ")")
        temp_corpus_topic_counts = np.zeros(num_topics)
        for potential_topic in range(num_topics):
            temp_corpus_topic_counts[potential_topic] = self.phi[:,potential_topic].sum()
        
        previous_timestamp = time.time()
        
        potentials = np.zeros(num_topics)
        
        # Iteration
        for iteration in range(num_iterations):
            for d in range(len(corpus.documents)):
                for i in range(corpus.documents[d].number_of_words):
                
                    # Remove assignment.
                    word = corpus.documents[d].ordered_word_list[i]
                    word_topic_counts_for_word = self.phi[corpus.ordered_vocabulary[word]]
                    
                    # Update Z
                    current_topic = int(self.z[d][i])
                    self.z[d][i] = -1
                    
                    # Update theta
                    author_id = corpus.documents[d].author_ids[0]
                    self.theta[author_id][current_topic] -= 1

                    word_topic_counts_for_word[current_topic] -= 1
                    temp_corpus_topic_counts[current_topic] -= 1
                    #print("Word: " + word + " at index " + str(corpus.ordered_vocabulary[word]))
                    
                    # Choose new assignment
                    # Iterate through topics
                    # TODO: missing smoothing param.
                    
                    theta_term = (self.theta[author_id] + smoothing) / (self.theta[author_id].sum() - 1 + num_topics * smoothing)
                    
                    denom_sum = temp_corpus_topic_counts 
                    
                    phi_term = (word_topic_counts_for_word + smoothing) / (denom_sum + smoothing * corpus.vocabulary_size)
                    
                    potentials = theta_term * phi_term
                    
                    # Apply new topic assignment
                    potentials = potentials / potentials.sum()
                    new_topic = int(np.random.choice(range(num_topics), p=potentials))
                    
                    # Update Z
                    self.z[d][i] = new_topic
                    
                    # Update Theta
                    self.theta[author_id][new_topic] += 1
                    temp_corpus_topic_counts[new_topic] += 1
                    
                    # Update Phi
                    word_topic_counts_for_word[new_topic] += 1
                    
            seconds_per_iteration = time.time() - previous_timestamp
            previous_timestamp = time.time()
            est_seconds_remaining = seconds_per_iteration * (num_iterations - iteration)
            est_minutes_remaining = int(est_seconds_remaining / 60)
            est_seconds_remaining = est_seconds_remaining % 60
            print ("Finished iteration " + str(iteration) + " / " + str(num_iterations) + " ETA: " +str(est_minutes_remaining) + "m " + str(est_seconds_remaining) + "s") 
        self.done_iterating = True
        
    def get_p_value(self, test_doc):
        """
        Use probabalistic nature of model to do determine most likely author for test_doc.
        """
        if not self.done_iterating:
            self.perform_iteration(self.corpus, self.num_topics, self.num_iterations)
        
        best_author_thus_far = None
        best_probability_thus_far = 0.0
        
        for author_id in self.corpus.authors.keys():
            probability = 1.0
            
            theta_author_cache = self.theta[author_id]
            theta_author_cache = theta_author_cache / theta_author_cache.sum()
            
            for w in range(len(test_doc.ordered_word_list)):
                word = test_doc.ordered_word_list[w]
                word_index = self.corpus.ordered_vocabulary[word]
                
                sum_term = 0
                phi_word_cache = self.phi[word_index]
                phi_word_cache = phi_word_cache / phi_word_cache.sum()
                
                sum_term = (theta_author_cache * phi_word_cache).sum()
                    
                probability *= sum_term
                #print("new_term: " + str(sum_term))
                #print("probability now: " + str(probability))
                assert(probability <= 1.0)
                
            if probability > best_probability_thus_far:
                #print ("New author at probability: " + str(probability))
                best_probability_thus_far = probability
                best_author_thus_far = author_id
                
        #fprint("Best author probability: " + str(best_probability_thus_far))
        return best_author_thus_far
