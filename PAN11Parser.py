import string
import xml.etree.ElementTree as ET
from BaseParser import BaseParser
from CorpusInfo import CorpusInfo
from DocumentInfo import DocumentInfo
from AuthorInfo import AuthorInfo
from Config import Config

class PAN11Parser (BaseParser):

    def __init__(self, training_dataset_file_name):
        self.training_dataset_file_name = training_dataset_file_name

    def get_corpus(self):
        print("PAN11 Parser GO!")
        
        result_corpus = CorpusInfo()
        
        # Load file
        file_contents = ""
        with open(self.training_dataset_file_name) as f:
            file_contents = f.read()
        
        
        # Mine file
        root = ET.fromstring (file_contents)
        for child in root:
            if len(result_corpus.documents) > Config.document_count_limit:
                result_corpus.finalize()
                return result_corpus
            
            author_id = child[0].attrib["id"]
            document_id = child.attrib["file"]
            document_text = child[1].text
            
            #Create structures
            new_document_info = DocumentInfo (document_id, document_text)
            if new_document_info.number_of_words <= 0:
                continue
            author_info = None
            if author_id in result_corpus.authors:
                author_info = result_corpus.authors[author_id]
            else:
                author_info = AuthorInfo (author_id)
                result_corpus.authors[author_id] = author_info
                
            #Fill in information
            author_info.documents_written.append(new_document_info)
            new_document_info.author_ids.append(author_id)
            result_corpus.documents.append(new_document_info)
            
        result_corpus.finalize()
        print("PAN11 Parser Finished")
        return result_corpus
        
        
    def get_validation_corpus(self):
        result_corpus = CorpusInfo()
        
        docs_by_id = {}
        
        # Load file
        validation_file_contents = ""
        ground_truth_file_contents = ""
        
        with open("pan11-author-identification-training-corpus-2011-04-08/LargeValid.xml") as f:
            validation_file_contents = f.read()
        
        with open("pan11-author-identification-training-corpus-2011-04-08/GroundTruthLargeValid.xml") as f:
            ground_truth_file_contents = f.read()
            
            
        # Create docs:
        root = ET.fromstring (validation_file_contents)
        for child in root:
            document_id = child.attrib["file"]
            document_text = child[1].text
            
            #Create structures
            new_document_info = DocumentInfo (document_id, document_text)
            if new_document_info.number_of_words <= 0:
                continue
                
            #Fill in information
            new_document_info.author_ids.append(author_id)
            result_corpus.documents.append(new_document_info)
            docs_by_id[document_id] = new_document_info
            
        # Assign Authors
        root = ET.fromstring (ground_truth_file_contents)
        for child in root:
            author_id = child[0].attrib["id"]
            document_id = child.attrib["file"]
            
            author_info = None
            
            if author_id in result_corpus.authors:
                author_info = result_corpus.authors[author_id]
            else:
                author_info = AuthorInfo (author_id)
                result_corpus.authors[author_id] = author_info
            
            doc = docs_by_id[document_id]
            doc.author_ids.append(author_id)
            author_info.documents_written.append(doc)
            
        result_corpus.finalize()
        return result_corpus