"""
Z (word-to-topic assignments) are the exact same as in LDA and AT.

Phi_d (word_topic_counts) for document topics.
Theta_d (topic_counts) topic counts per document. So shape = (num_docs, mum_document_topics)

phi_a (word_topic_counts) for author topics.
theta_d (topic counts) topic counts per author. Shape = (num_authors, num_author_topics)

Y (word-type assignments) dictate whether words pull from the document or author side of the model.
0: document
1: author
"""

import numpy as np
import time
from collections import Counter, OrderedDict
from yprint import yprint, fprint
from BaseModel import BaseModel
from DirichletDistribution import beta, bernoulli
from Config import Config

# PARAMS
smoothing = 0.1

class DADTModel(BaseModel):      
    
    def __init__(self, corpus, num_document_topics, num_author_topics, num_iterations):
        
        self.corpus = corpus
        self.num_document_topics = num_document_topics
        self.num_author_topics = num_author_topics
        self.num_iterations = num_iterations
        self.done_iterating = False
  
        # Phi_d Setup
        self.phi_d = np.zeros(shape=(corpus.vocabulary_size, num_document_topics))
        
        # Phi_a Setup
        self.phi_a = np.zeros(shape=(corpus.vocabulary_size, num_author_topics))
        
        # Theta_d Setup
        self.theta_d = []
        for doc in corpus.documents:
            self.theta_d.append(np.zeros(num_document_topics))
        
        # Theta_a Setup
        self.theta_a = {} 
        for author_id in corpus.authors.keys():
            self.theta_a[author_id] = np.zeros(num_author_topics)
        
        # Y Setup
        self.y = list()
        for i in range(len(corpus.documents)):
            self.y.append(np.zeros(corpus.documents[i].number_of_words))
        
        # Z Setup
        self.z = list()
        for i in range(len(corpus.documents)):
            self.z.append(np.zeros(corpus.documents[i].number_of_words))
  
    def get_description_string():
        return "num_topics: [" + str() + "]"; 

    def initialize(self):
        # Initialization
        yprint ("[BEGIN DADT]")
        yprint ("Initialization...")
        for d in range(len(self.corpus.documents)):
            for i in range(self.corpus.documents[d].number_of_words):
                # Update Z + Y
                pi = np.random.beta(Config.delta_a, Config.delta_d)
                yi = bernoulli(pi)
                
                t = None
                if yi == 0:
                    t = np.random.randint(self.num_document_topics)
                else:
                    t = np.random.randint(self.num_author_topics)
                
                self.z[d][i] = t
                self.y[d][i] = yi
                
                # Update Theta
                if yi == 0:
                    self.theta_d[d][t] += 1
                else:
                    author_id = self.corpus.documents[d].author_ids[0]
                    self.theta_a[author_id][t] += 1
                
                # Update Phi
                word = self.corpus.documents[d].ordered_word_list[i]
                if yi == 0:
                    self.phi_d[self.corpus.ordered_vocabulary[word]][t] += 1
                else:
                    self.phi_a[self.corpus.ordered_vocabulary[word]][t] += 1
    
        yprint("DADT Setup Complete!")
        
    def get_training_data(self):
        """
        Concatenate Document and Author topic distros.
        """
        if not self.done_iterating:
            self.perform_iteration(self.corpus, self.num_iterations)
        
        X = list()
        y = list()
        for d in range(len(self.corpus.documents)):
            X.append(np.concatenate((self.theta_d[d], self.theta_a[self.corpus.documents[d].author_ids[0]])))
            y.append(self.corpus.documents[d].author_ids[0])
        
        return X, y
        
    def perform_iteration(self, corpus, num_iterations):
        print ("Iteration")
        
        # Setup Phi Caches
        temp_corpus_topic_counts_d = np.zeros(self.num_document_topics)
        for potential_topic in range(self.num_document_topics):
            temp_corpus_topic_counts_d[potential_topic] = self.phi_d[:,potential_topic].sum()
        
        temp_corpus_topic_counts_a = np.zeros(self.num_author_topics)
        for potential_topic in range(self.num_author_topics):
            temp_corpus_topic_counts_a[potential_topic] = self.phi_a[:,potential_topic].sum()
        
        previous_timestamp = time.time()
        
        int_cast = int
        
        # Iteration
        for iteration in range(num_iterations):
        
            # For every document in the corpus...
            for d in range(len(corpus.documents)):
            
                # For every word in the document...
                for i in range(corpus.documents[d].number_of_words):
                
                    # Remove assignment.
                    word = corpus.documents[d].ordered_word_list[i]
                    author_id = corpus.documents[d].author_ids[0]
                    
                    # If the word is currently pegged as a document word, we must subtract from self.phi_d. Otherwise, self.phi_a
                    # This implies we need to track word types.
                    # Need array of shape (documents, num_words_in_document) to track type.
                    
                    # Grab data for current word
                    word_topic_counts_for_word = None
                    theta = None
                    theta_index = None
                    temp_corpus_topic_counts = None
                    num_topics = None
                    
                    
                    if self.y[d][i] == 0:
                        word_topic_counts_for_word = self.phi_d[corpus.ordered_vocabulary[word]]
                        theta = self.theta_d
                        theta_index = d
                        temp_corpus_topic_counts = temp_corpus_topic_counts_d
                        num_topics = self.num_document_topics
                    else:
                        word_topic_counts_for_word = self.phi_a[corpus.ordered_vocabulary[word]]
                        theta = self.theta_a
                        theta_index = author_id
                        temp_corpus_topic_counts = temp_corpus_topic_counts_a
                        num_topics = self.num_author_topics
                    
                    # Update Z
                    current_topic = int_cast(self.z[d][i])
                    self.z[d][i] = -1
                    
                    # Update theta 
                    theta[theta_index][current_topic] -= 1
                    word_topic_counts_for_word[current_topic] -= 1
                    temp_corpus_topic_counts[current_topic] -= 1
                    
                    # Update Y
                    self.y[d][i] = -1
                    
                    # Generate new data for current word
                    pi = np.random.beta(Config.delta_a, Config.delta_d)
                    yi = bernoulli(pi)
                    self.y[d][i] = yi
                    if self.y[d][i] == 0:
                        word_topic_counts_for_word = self.phi_d[corpus.ordered_vocabulary[word]]
                        theta = self.theta_d
                        theta_index = d
                        temp_corpus_topic_counts = temp_corpus_topic_counts_d
                        num_topics = self.num_document_topics
                    else:
                        word_topic_counts_for_word = self.phi_a[corpus.ordered_vocabulary[word]]
                        theta = self.theta_a
                        theta_index = author_id
                        temp_corpus_topic_counts = temp_corpus_topic_counts_a
                        num_topics = self.num_author_topics
                    
                    
                    theta_sum = theta[theta_index].sum()
                    theta_term = (theta[theta_index] + smoothing) / (theta_sum - 1 + num_topics * smoothing)
                    denom_sum = temp_corpus_topic_counts
                    phi_term = (word_topic_counts_for_word + smoothing) / (denom_sum + smoothing * corpus.vocabulary_size)
                    potentials = theta_term * phi_term
                    # Choose new assignment
                    # Iterate through topics
                    # TODO: missing smoothing param.
                    
                    
                    
                    # Apply new topic assignment
                    potentials = potentials / potentials.sum()
                    new_topic = int_cast(np.random.choice(range(num_topics), p=potentials))
                    
                    # Update Z
                    self.z[d][i] = new_topic
                    
                    # Update Theta
                    theta[theta_index][new_topic] += 1
                    temp_corpus_topic_counts[new_topic] += 1
                    
                    # Update Phi
                    word_topic_counts_for_word[new_topic] += 1
                    
            seconds_per_iteration = time.time() - previous_timestamp
            previous_timestamp = time.time()
            est_seconds_remaining = seconds_per_iteration * (num_iterations - iteration)
            est_minutes_remaining = int_cast(est_seconds_remaining / 60)
            est_seconds_remaining = est_seconds_remaining % 60
            print ("Finished iteration " + str(iteration) + " / " + str(num_iterations) + " ETA: " +str(est_minutes_remaining) + "m " + str(est_seconds_remaining) + "s") 
        self.done_iterating = True
    
    def get_topic_for_word_index(word_index):
        self.phi_d
    
    def get_p_value(self, test_doc):
        """
        Use probabalistic nature of model to do determine most likely author for test_doc.
        """
        if not self.done_iterating:
            self.perform_iteration(self.corpus, self.num_iterations)
        
        best_author_thus_far = None
        best_probability_thus_far = 0.0
        
        pi = np.random.beta(Config.delta_a, Config.delta_d)
        num_docs = len(self.corpus.documents)
        
        v = 0
        total = 0
        for t in range(self.num_document_topics):
            for d in range(num_docs):
                total += self.theta_d[d][t]
        v = 1 
        # What is "v"?
        # How much the documents like this word.
        # Should be different for each value of "t"
        
        
        # sum_term_d is
        # How much is the topic liked by all the existing documents?
        # *
        # Which topic does the word most likely belong to?
        #
        author_ind = 0
        for author_id in self.corpus.authors.keys():
            probability = 1.0
            
            
            theta_author_cache = self.theta_a[author_id]
            theta_author_cache = theta_author_cache / theta_author_cache.sum()
        
            
            for w in range(len(test_doc.ordered_word_list)):
                word = test_doc.ordered_word_list[w]
                word_index = self.corpus.ordered_vocabulary[word]
                
                
                
                phi_author_cache = self.phi_a[word_index]
                if phi_author_cache.sum() <= 0:
                    continue
                phi_author_cache = phi_author_cache / phi_author_cache.sum()
                
                sum_term_a = (theta_author_cache * phi_author_cache).sum() # Which authors do we like * Which topics do we like

                
                #sum_term_d = self.theta_d[d] ## TODO: Parallelize this double for loop!
                
                if(self.phi_d[word_index].sum() <= 0):
                    continue
                
                phi_document_cache = self.phi_d[word_index]
                phi_document_cache = phi_document_cache / phi_document_cache.sum()
                #print("phi_doc_nan: " + str(phi_document_cache))
                #print("other_nan: " + str(self.phi_d[word_index]))
                #sum_term_d += v * self.phi_d[word_index][t]
               
                sum_term_d = v * phi_document_cache.sum()
                #print("v: " + str(v))
                
                
  
                new_term = pi * sum_term_a + (1.0 - pi) * sum_term_d
                probability *= new_term

                
                #print("sum_term_d: " + str(sum_term_d) + " sum_term_a: " + str(sum_term_a))
                
                
                #print("new_term: " + str(new_term))
                #print("probability now: " + str(probability))
                #assert(probability <= 1.0)
                #print ("ho")
                
            if probability > best_probability_thus_far:
                best_probability_thus_far = probability
                best_author_thus_far = author_id
            
        
            #print("Finished Author " + str(author_ind) + " / " + str(len(self.corpus.authors.keys())))
            #author_ind += 1
                
        #fprint("Best author probability: " + str(best_probability_thus_far))
        return best_author_thus_far
    

    
    