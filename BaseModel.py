class BaseModel:
    def get_description_string(self):
        return "<get_description_string not implemented>"
        
    def initialize(self, corpus):
        self.corpus = corpus
    
    def get_training_data(self):
        fprint("[ERROR] get_training_data function not implemented")
    
    def get_p_value(self, test_doc, corpus):
        """
        Use probabalistic nature of model to do determine most likely author for test_doc.
        """
        print("get_p_value not implemented!")
        assert(False)
        
    def get_training_data_from_corpus(self, other_corpus):
        """
        Convert corpus into classifier-ready training data.
        """
        print("get_training_data_from_corpus not implemented!")
        assert(False)
        