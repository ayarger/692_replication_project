import time
import os
import Config

verbose = False
file_handler = None


def setverbosity(b):
    verbose = b

def yprint(s):
    if verbose:
        print(str(s))
        
def fprint(s):
    global file_handler
    print (s)
    if file_handler == None:
        filename = Config.get_filename() + ".txt"
        file_handler = open(filename, 'w')
    file_handler.write(s + "\n")
    
def flush_printing():
    global file_handler
    if file_handler is not None:
        file_handler.close()
    