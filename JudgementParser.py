import string
from BaseParser import BaseParser
from CorpusInfo import CorpusInfo
from DocumentInfo import DocumentInfo
from AuthorInfo import AuthorInfo
 
class JudgementParser (BaseParser):

    def __init__(self, dataset_file):
        self.dataset_file = dataset_file

    def get_corpus(self):
        print ("Judgement Parser GO")
        result_corpus = CorpusInfo()
        file_contents = None
        punct = set(string.punctuation)
        
        # Load file
        with open(self.dataset_file) as f:
            file_contents = f.read()
        
        if file_contents == None:
            print("[ERROR] Dataset file " + self.dataset_file + " needs to be in the same directory as main.py.")
        
        lines = file_contents.splitlines()
        
        # Prepare lines    
        for line in lines:
            tokens = line.split()
            author_id = tokens[0]
            document_id = tokens[1]
            
            # Create new DocumentInfo
            new_document_info = DocumentInfo(document_id, line)
            if new_document_info.number_of_words <= 0:
                continue
            author_info = None
            
            # Create new AuthorInfo
            if author_id in result_corpus.authors:
                author_info = result_corpus.authors[author_id]
            else:
                author_info = AuthorInfo(author_id)
                result_corpus.authors[author_id] = author_info
                
            # Fill in information
            author_info.documents_written.append(new_document_info)
            new_document_info.author_ids.append(author_id)
            result_corpus.documents.append(new_document_info)
        
        result_corpus.finalize()
        print ("Judgement Parser Finished")
        
        return result_corpus
        
        