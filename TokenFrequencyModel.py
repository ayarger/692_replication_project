from numpy import zeros
from BaseModel import BaseModel
from yprint import yprint, setverbosity
import numpy as np

class TokenFrequencyModel(BaseModel):
    def __init__(self, corpus):
        self.corpus = corpus

    def get_description_string(self):
        return "...";
        
    def initialize(self):
        pass
    
    # Return list of 2-tuples in the form (WordFrequencyDistro, author_numeric_id)
    def get_training_data(self):
        data_points = []
        labels = []
        
        # Build words-to-index map for performance
        words_to_index = {}
        i = 0
        for word in self.corpus.ordered_vocabulary:
            words_to_index[word] = i
            i += 1
            
        # Mine frequencies
        for doc in self.corpus.documents:
            #for word in self.corpus.ordered_vocabulary:
            document_word_inventory_array = zeros(self.corpus.vocabulary_size)
            for word in doc.ordered_word_list:
                ind = words_to_index[word]
                document_word_inventory_array[ind] += 1.0
            
            document_word_inventory_array /= doc.number_of_words
            
            #document_word_inventory_array = itemfreq(doc.ordered_word_list)[:,1] / len(doc.ordered_word_list)
            #    document_word_inventory_array.append(doc.ordered_word_list.count(word) / doc.number_of_words) # POSSIBLE ENHANCEMENT: cleanup words? All to lowercase / typoes fixed?
            
            yprint(sum(document_word_inventory_array))
            #assert(sum(document_word_inventory_array) == 1.0)
            
            data_points.append (document_word_inventory_array)
            labels.append (self.corpus.authors[doc.author_ids[0]].numeric_id)

        assert len(data_points) == len(labels)
            
        return np.array(data_points), np.array(labels)
        
    def convert_test_set(self, test_set_X):
        """
        Convert test_set of documents into inputs to the model.
        """
        result = list()
        # Need each document to become an array of token frequencies.
        for doc in test_set_X:
            doc_rep = np.zeros(self.corpus.vocabulary_size)
            token_index = 0
            for token in self.corpus.ordered_vocabulary.keys():
                token_frequency = doc.ordered_word_list.count(token) / doc.number_of_words
                doc_rep[token_index] = token_frequency
                token_index += 1
            
            result.append(doc_rep)
            
        result = np.array(result)
        return result
        