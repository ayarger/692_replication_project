class BaseMethod:
    def get_description_string(self):
        print ("<get_description_string not implemented>")
        assert(False)
        
    def test(self, test_set):
        """
        Evaluate the trained model on a test set.
        RETURNS: correct-guess percentage.
        """
        print ("<test not implemented>")
        assert(False)
        
    
    def validate(self, num_folds):
        """
        Run num_folds cross validation on the trained model.
        RETURNS: Validation Score.
        """
        print ("<validate not implemented>")
        assert(False)