import numpy as np
from yprint import fprint
from BaseMethod import BaseMethod
from ATModel import ATModel
from DADTModel import DADTModel

class PMethod (BaseMethod):
    
    def __init__(self, model):
        self.model = model
    
    def test(self, test_set):
        """
        Evaluate the trained model on a test set.
        RETURNS: Correct-guess percentage.
        """
        if type(self.model) == ATModel:
            return _test_at()
        elif type(self.model) == DADTModel:
            return _test_dadt()
            
        print ("Failed to evaluate model of type [" + str(type(self.model)) + "]")
        assert(False)
        return -1
        
    def _test_at(self):
        return -1
        
    def _test_dadt(self):
        return -1
    
    def validate(self, num_folds):
        """
        Run num_folds cross validation on the trained model.
        RETURNS: Validation Score.
        """
        fprint("Validating...")
        num_attempts = 0
        num_correct = 0
        
        for doc in self.model.corpus.documents:
            print("num_attempts: " + str(num_attempts) + " / " + str(len(self.model.corpus.documents)))
            
            author_guess = self.model.get_p_value(doc)
            #print("guess: " + str(author_guess))
            if doc.author_ids[0] == author_guess:
                num_correct += 1
            num_attempts += 1
        
        scores = [num_correct / num_attempts]
        
        fprint("Individual Validation Scores:")
        fprint(str(scores))
       
        return np.average(scores)
        
    def validate_on_other_corpus(self, valid_corpus):
        fprint("Validating...")
        num_attempts = 0
        num_correct = 0
        
        for doc in valid_corpus.documents:
            print("num_attempts: " + str(num_attempts) + " / " + str(len(valid_corpus.documents)))
            
            author_guess = self.model.get_p_value(doc)
            print("guess: " + str(author_guess))
            if doc.author_ids[0] == author_guess:
                num_correct += 1
            num_attempts += 1
        
        scores = [num_correct / num_attempts]
        
        fprint("Other-Corpus Validation Scores:")
        fprint(str(scores))
       
        return np.average(scores)