class AuthorInfo:
    next_numeric_id = 0
    
    def __init__(self, id):
        self.id = id
        self.numeric_id = AuthorInfo.next_numeric_id
        AuthorInfo.next_numeric_id += 1
        self.documents_written = []
        
class AuthorParamsInfo:
    def __init__(self, num_author_topics=5, author_topic_prior=[], word_in_author_topic_prior=[], author_in_corpus_prior=[], author_words_in_doc_prior=5):
        self.num_author_topics = num_author_topics
        self.author_topic_prior = author_topic_prior
        self.word_in_author_topic_prior = word_in_author_topic_prior
        self.author_in_corpus_prior = author_in_corpus_prior
        self.author_words_in_doc_prior = author_words_in_doc_prior