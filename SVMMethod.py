from sklearn import svm
from sklearn.model_selection import cross_val_score
import numpy as np
from sklearn.utils import shuffle
from yprint import yprint, fprint
from BaseMethod import BaseMethod
from TokenFrequencyModel import TokenFrequencyModel

class SVMMethod(BaseMethod):
    
    def __init__(self, model):
        self.model = model
        self.classifier = svm.LinearSVC()
        
        X, y = model.get_training_data()
        self.classifier.fit(X, y)
      
    def test(self, test_set):
        """
        Evaluate the trained model on a test set.
        RETURNS: correct-guess percentage.
        """
        print ("<test not implemented>")
        assert(False)
        
    def validate(self, num_folds):
        """
        Run num_folds cross validation on the trained model.
        RETURNS: Validation Score.
        """
        
        X, y = self.model.get_training_data()
        print ("validating SVM Method")
        
        assert(len(X) == len(y))
       
        scores = cross_val_score (self.classifier, X, y, cv=num_folds, n_jobs=1, pre_dispatch=1)
        yprint("Individual Validation Scores:")
        yprint(scores)
       
        return np.average(scores)
        
    def validate_on_other_corpus(self, valid_corpus):
        fprint("Validating...")
        
        X, y = self.model.get_training_data_from_corpus(valid_corpus)
        
        scores = cross_val_score (self.classifier, X, y, cv=num_folds, n_jobs=1, pre_dispatch=1)
        yprint("Individual Validation Scores:")
        yprint(scores)

        fprint(str(scores))
       
        return np.average(scores)